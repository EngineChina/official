import React from 'react';
import { Button } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import QueueAnim from 'rc-queue-anim';
import TweenOne, { TweenOneGroup } from 'rc-tween-one';
import BannerAnim, { Element } from 'rc-banner-anim';
import { isImg } from './utils';
import 'rc-banner-anim/assets/index.css';

const { BgElement } = Element;
class Banner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          bg: "https://zos.alipayobjects.com/rmsportal/gGlUMYGEIvjDOOw.jpg",
          title: require("../../assets/images/bannerLogo.png"),
          content: "云团队，铸就别番风味",
          button: "Learn More"
        },{
          bg: "https://zos.alipayobjects.com/rmsportal/xHxWkcvaIcuAdQl.jpg",
          title: require("../../assets/images/bannerLogo.png"),
          content: "我们拥有高效的线上开发效率",
          button: "Learn More"
        },{
          bg: "https://zos.alipayobjects.com/rmsportal/hzPBTkqtFpLlWCi.jpg",
          title: require("../../assets/images/bannerLogo.png"),
          content: "让你的网页脱颖而出",
          button: "Learn More"
        },
      ]
    }
  }

  componentDidMount() {
    setInterval(()=>{
      document.getElementsByClassName("next")[0].click();
    },5000)
  }

  render() {
    const that = this;
    const { ...props } = this.props;
    const dataSource = {
      wrapper: { className: 'banner1' },
      BannerAnim: {
        children: that.state.data.map((v,i)=>({
          name: 'elem'+i,
          BannerElement: { className: 'banner-user-elem' },
          textWrapper: { className: 'banner1-text-wrapper' },
          bg: {
            className: 'bg' ,
            style:{
              "background": "url(" + v.bg +") center"
            }
          },
          title: {
            className: 'banner1-title',
            children: (
              <img src={v.title} width="100%" alt="img"/>
            ),
          },
          content: {
            className: 'banner1-content',
            children: v.content,
          },
          button: {
            className: 'banner1-button',
            onClick: function () {
              document.getElementById("ProductsAndServices").scrollIntoView({
                behavior: "smooth",
                block: "start"
              })
            },
            children: v.button },
        })),
      },
    };
    delete props.dataSource;
    delete props.isMobile;
    const childrenToRender = dataSource.BannerAnim.children.map((item, i) => {
      const elem = item.BannerElement;
      const elemClassName = elem.className;
      delete elem.className;
      const { bg, textWrapper, title, content, button } = item;
      return (
        <Element key={i.toString()} {...elem} prefixCls={elemClassName}>
          <BgElement key="bg" {...bg} />
          <QueueAnim
            type={['bottom', 'top']}
            delay={200}
            key="text"
            {...textWrapper}
          >
            <div key="logo" {...title}>
              {typeof title.children === 'string' &&
              title.children.match(isImg) ? (
                <img src={title.children} width="100%" alt="img" />
              ) : (
                title.children
              )}
            </div>
            <div key="content" {...content}>
              {content.children}
            </div>
            <Button ghost key="button" {...button}>
              {button.children}
            </Button>
          </QueueAnim>
        </Element>
      );
    });
    return (
      <div {...props} {...dataSource.wrapper}>
        <TweenOneGroup
          key="bannerGroup"
          enter={{ opacity: 0, type: 'from' }}
          leave={{ opacity: 0 }}
          component=""
        >
          <div className="banner1-wrapper" key="wrapper">
            <BannerAnim key="BannerAnim" {...dataSource.BannerAnim}>
              {childrenToRender}
            </BannerAnim>
          </div>
        </TweenOneGroup>
        <TweenOne
          animation={{
            y: '-=20',
            yoyo: true,
            repeat: -1,
            duration: 1000,
          }}
          className="banner1-icon"
          style={{ bottom: 40 }}
          key="icon"
        >
          <DownOutlined />
        </TweenOne>
      </div>
    );
  }
}

export default Banner;
