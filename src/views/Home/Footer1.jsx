import React from 'react';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import QueueAnim from 'rc-queue-anim';
import { Row, Col } from 'antd';
import { getChildrenToRender } from './utils';
import { isImg } from './utils';

class Footer extends React.Component {
  static defaultProps = {
    className: 'footer1',
  };

  getLiChildren = (data) =>
    data.map((item, i) => {
      const { title, childWrapper, ...itemProps } = item;
      return (
        <Col key={i.toString()} {...itemProps} title={null} content={null}>
          <h2 {...title}>
            {typeof title.children === 'string' &&
            title.children.match(isImg) ? (
              <img src={title.children} width="100%" alt="img" />
            ) : (
              title.children
            )}
          </h2>
          <div {...childWrapper}>
            {childWrapper.children.map(getChildrenToRender)}
          </div>
        </Col>
      );
    });

  render() {
    const { ...props } = this.props;
    const dataSource = {
      wrapper: { className: 'home-page-wrapper footer1-wrapper' },
      OverPack: { className: 'footer1', playScale: 0.2 },
      block: {
        className: 'home-page',
        gutter: 0,
        children: [
          {
            name: 'block0',
            xs: 24,
            md: 6,
            className: 'block',
            title: {
              className: 'logo',
              children: (<img src={require("../../assets/images/FooterLogo.png")} width="100%" alt="img"/>),
            },
            childWrapper: {
              className: 'slogan',
              children: [
                {
                  name: 'content0',
                  children: 'Engine China is a professional cloud team.',
                },
                {
                  name: "content1",
                  children: [
                    <img key='contentImg0' src={require("../../assets/images/code1.png")} width="40%" alt="img"/>,
                    <img key='contentImg1' src={require("../../assets/images/code1.png")} width="40%" alt="img"/>
                  ]
                },
                {
                  name: "content1",
                  children: [
                    <p key='contentMsg0'>微信公众号</p>,
                    <p key='contentMsg1'>客服微信</p>
                  ]
                }
              ],
            },
          },
          {
            name: 'block1',
            xs: 24,
            md: 6,
            className: 'block',
            title: { children: '产品' },
            childWrapper: {
              children: [
                { name: 'link0', href: '#', children: '产品更新记录' },
                { name: 'link1', href: '#', children: 'API文档' },
                { name: 'link2', href: '#', children: '快速入门' },
                { name: 'link3', href: '#', children: '参考指南' },
              ],
            },
          },
          {
            name: 'block2',
            xs: 24,
            md: 6,
            className: 'block',
            title: { children: '关于' },
            childWrapper: {
              children: [
                { href: '#', name: 'link0', children: 'FAQ' },
                { href: '#', name: 'link1', children: '联系我们' },
              ],
            },
          },
          {
            name: 'block3',
            xs: 24,
            md: 6,
            className: 'block',
            title: { children: '资源' },
            childWrapper: {
              children: [
                { href: '#', name: 'link0', children: 'Ant Design' },
                { href: '#', name: 'link1', children: 'Ant Motion' },
              ],
            },
          },
        ],
      },
      copyrightWrapper: { className: 'copyright-wrapper' },
      copyrightPage: { className: 'home-page' },
      copyright: {
        className: 'copyright',
        children: (
          <span>
        ©2018 by <a href="/#">Engine China</a> All Rights
        Reserved
      </span>
        ),
      },
    };
    delete props.dataSource;
    delete props.isMobile;
    const childrenToRender = this.getLiChildren(dataSource.block.children);
    return (
      <div {...props} {...dataSource.wrapper}>
        <OverPack {...dataSource.OverPack}>
          <QueueAnim
            type="bottom"
            key="ul"
            leaveReverse
            component={Row}
            {...dataSource.block}
          >
            {childrenToRender}
          </QueueAnim>
          <TweenOne
            animation={{ y: '+=30', opacity: 0, type: 'from' }}
            key="copyright"
            {...dataSource.copyrightWrapper}
          >
            <div {...dataSource.copyrightPage}>
              <div {...dataSource.copyright}>
                {dataSource.copyright.children}
              </div>
            </div>
          </TweenOne>
        </OverPack>
      </div>
    );
  }
}

export default Footer;
