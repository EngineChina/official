import React from 'react';
import { Row, Col } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import { getChildrenToRender } from './utils';

class Thanks extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {name: "阿里巴巴", img: "https://s.alicdn.com/@img/tfs/TB1pDDmmF67gK0jSZPfXXahhFXa-2814-380.png"},
        {name: "七牛云", img: require("../../assets/test/qly.png")},
        {name: "知乎", img: require("../../assets/test/zh.png")},
        {name: "蚂蚁金服", img: require("../../assets/test/myjf.png")},
        {name: "CSDN", img: require("../../assets/test/csdn.png")},
        {name: "知己基因", img: "https://gw.alipayobjects.com/zos/rmsportal/TFicUVisNHTOEeMYXuQF.svg"}
      ]
    }
  }

  getChildrenToRender = (data) =>
    data.map((item) => {
      return (
        <Col key={item.name} {...item}>
          <div {...item.children.wrapper}>
            <span {...item.children.img}>
              <img src={item.children.img.children} alt="img" />
            </span>
          </div>
        </Col>
      );
    });

  render() {
    let that = this;
    const { ...props } = this.props;
    let dataSource  = {
      wrapper: { className: 'home-page-wrapper content12-wrapper' },
      page: { className: 'home-page content12' },
      OverPack: { playScale: 0.3, className: '' },
      titleWrapper: {
        className: 'title-wrapper',
        children: [
          {
            name: 'image',
            children:
              'https://gw.alipayobjects.com/zos/rmsportal/PiqyziYmvbgAudYfhuBr.svg',
            className: 'title-image',
          },
          { name: 'title', children: '特别鸣谢', className: 'title-h1' },
        ],
      },
      block: {
        className: 'img-wrapper',
        children: that.state.data.map((v,i)=>{
          return  {
            name: 'block'+i,
            className: 'block',
            md: 8,
            xs: 24,
            children: {
              wrapper: { className: 'block-content' },
              img: {
                children: v.img,
              },
            },
          }
        }),
      },
    };
    delete props.dataSource;
    delete props.isMobile;
    const childrenToRender = this.getChildrenToRender(
      dataSource.block.children
    );
    return (
      <div {...props} {...dataSource.wrapper}>
        <div {...dataSource.page}>
          <div key="title" {...dataSource.titleWrapper}>
            {dataSource.titleWrapper.children.map(getChildrenToRender)}
          </div>
          <OverPack
            className={`content-template ${props.className}`}
            {...dataSource.OverPack}
          >
            <TweenOneGroup
              component={Row}
              key="ul"
              enter={{
                y: '+=30',
                opacity: 0,
                type: 'from',
                ease: 'easeOutQuad',
              }}
              leave={{ y: '+=30', opacity: 0, ease: 'easeOutQuad' }}
              {...dataSource.block}
            >
              {childrenToRender}
            </TweenOneGroup>
          </OverPack>
        </div>
      </div>
    );
  }
}

export default Thanks;
