/* eslint no-undef: 0 */
/* eslint arrow-parens: 0 */
import React from 'react';
import { enquireScreen } from 'enquire-js';

import ToTop from "../../components/ToTop";
import Nav from './Nav';
import Banner from './Banner';
import ProductsAndServices from './ProductsAndServices';
import Advantage from './Advantage';
import ProfessionalService from './ProfessionalService';
import Pricing from './Pricing';
import Case from './Case';
import Teams from './Teams';
import Thanks from './Thanks';
import ContactUs from './ContactUs';
import Footer1 from './Footer1';

import './less/antMotionStyle.less';

let isMobile;
enquireScreen((b) => {
  isMobile = b;
});

const { location } = window;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile,
      show: !location.port, // 如果不是 dva 2.0 请删除
    };
  }

  componentDidMount() {
    // 适配手机屏幕;
    enquireScreen((b) => {
      this.setState({ isMobile: !!b });
    });
    // dva 2.0 样式在组件渲染之后动态加载，导致滚动组件不生效；线上不影响；
    /* 如果不是 dva 2.0 请删除 start */
    if (location.port) {
      // 样式 build 时间在 200-300ms 之间;
      setTimeout(() => {
        this.setState({
          show: true,
        });
      }, 500);
    }
    /* 如果不是 dva 2.0 请删除 end */
  }

  render() {
    const children = [
      <ToTop
        id="ToTop"
        key="ToTop"
        showScroll={1000}
      >
      </ToTop>,
      <Nav
        id="Nav"
        key="Nav"
        isMobile={this.state.isMobile}
      />,
      <Banner
        id="Banner"
        key="Banner"
        isMobile={this.state.isMobile}
      />,
      <ProductsAndServices
        id="ProductsAndServices"
        key="ProductsAndServices"
        isMobile={this.state.isMobile}
      />,
      <Advantage
        id="Advantage"
        key="Advantage"
        isMobile={this.state.isMobile}
      />,
      <ProfessionalService
        id="ProfessionalService"
        key="ProfessionalService"
        isMobile={this.state.isMobile}
      />,
      <Pricing
        id="Pricing"
        key="Pricing"
        isMobile={this.state.isMobile}
      />,
      <Case
        id="Case"
        key="Case"
        isMobile={this.state.isMobile}
      />,
      <Teams
        id="Teams"
        key="Teams"
        isMobile={this.state.isMobile}
      />,
      <Thanks
        id="Thanks"
        key="Thanks"
        isMobile={this.state.isMobile}
      />,
      <ContactUs
        id="ContactUs"
        key="ContactUs"
        isMobile={this.state.isMobile}
      />,
      <Footer1
        id="Footer"
        key="Footer"
        isMobile={this.state.isMobile}
      />,
    ];
    return (
      <div
        className="templates-wrapper"
        ref={(d) => {
          this.dom = d;
        }}
      >
        {/* 如果不是 dva 2.0 替换成 {children} start */}
        {this.state.show && children}
        {/* 如果不是 dva 2.0 替换成 {children} end */}
      </div>
    );
  }
}
