import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { Row, Col } from 'antd';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import { getChildrenToRender } from './utils';

class Teams extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      //一段成员的假数据
      data:[
        {
          name:"幻城",
          intro:"灵魂程序员#一杯茶，一包烟，一行代码写一天…",
          portrait:"https://static.dingtalk.com/media/lADPBGnDYvC6-ZDNB-DNCoQ_2692_2016.jpg"
        },
        {
          name:"大凡",
          intro:"激进主义者",
          portrait:"https://static.dingtalk.com/media/lADPDh0cLtnJKMbNArLNArI_690_690.jpg"
        },{
          name:"信仰",
          intro:"A java developer",
          portrait:"https://static.dingtalk.com/media/lADPDefRwzHEcsLNAgDNAgA_512_512.jpg"
        },{
          name:"南风",
          intro:"小的事情都处理好，这样大的事情就不会爆炸。独当一面、就算痛哭流涕也没关系！",
          portrait:"https://static.dingtalk.com/media/lADPGoU8ckV5idvNA8DNBQA_1280_960.jpg"
        },{
          name:"天净沙",
          intro:"一个小小白₍₍ (̨̡ ‾᷄ᗣ‾᷅ )̧̢ ₎₎",
          portrait:"https://static.dingtalk.com/media/lADPDf0iuwmCc__NAoDNAoA_640_640.jpg"
        },{
          name:"东东",
          intro:"战斗民族的继承者",
          portrait:"https://static.dingtalk.com/media/lADPDfYHvb_5XAvNA7bNA7c_951_950.jpg"
        },{
          name:"丹阳",
          intro:"对IT比较执着哈哈。",
          portrait:"https://static.dingtalk.com/media/lADPDfJ6Pxsna5HNBDjNBDg_1080_1080.jpg"
        },{
          name:"月夕",
          intro:"遇者千虑，必有一得。一切随缘",
          portrait:require("../../assets/test/yx.jpg")
        },{
          name:"潜龙勿用",
          intro:"即使走的慢也绝不后退",
          portrait:"https://static.dingtalk.com/media/lADPGpb_63TrIfnNA7_NA78_959_959.jpg"
        },{
          name:"Hantme",
          intro:"一个探索前端的小石子",
          portrait:"https://static.dingtalk.com/media/lADPDe7swHFnanXNAyDNA8A_960_800.jpg"
        },{
          name:"大白",
          intro:"努力做最帅的工程师",
          portrait:require("../../assets/test/db.png")
        },{
          name:"丰星星",
          intro:"又菜又爱写前端",
          portrait:require("../../assets/test/fxx.png")
        }
      ],
    }
  }
  getBlockChildren = (data) =>
    data.map((item, i) => {
      const { titleWrapper, image, ...$item } = item;
      return (
        <Col key={i.toString()} {...$item}>
          <Row>
            <Col span={7}>
              <div {...image}>
                <img alt="img" src={image.children} />
              </div>
            </Col>
            <Col span={17}>
              <QueueAnim {...titleWrapper} type="bottom">
                {titleWrapper.children.map(getChildrenToRender)}
              </QueueAnim>
            </Col>
          </Row>
        </Col>
      );
    });

  render() {
    const that = this;
    const { ...props } = this.props;
    //需要渲染的数据
    const dataSource  = {
      wrapper: { className: 'home-page-wrapper teams2-wrapper' },
      page: { className: 'home-page teams2' },
      OverPack: { playScale: 0.3, className: '' },
      titleWrapper: {
        className: 'title-wrapper',
        children: [{ name: 'title', children: '团队成员' }],
      },
      block: {
        className: 'block-wrapper',
        gutter: 72,
        children: that.state.data.map((v,i)=>{
          return {
            name: 'block'+i,
            className: 'block',
            md: 8,
            xs: 24,
            image: {
              name: 'image',
              className: 'teams2-image',
              //头像
              children:
              v.portrait,
            },
            titleWrapper: {
              className: 'teams2-textWrapper',
              children: [
                //花名
                { name: 'title', className: 'teams2-title', children: v.name },
                //简介
                {
                  name: 'content',
                  className: 'teams2-job',
                  children: v.intro,
                }/*,
                  {
                    name: 'content1',
                    className: 'teams2-content',
                    children: 'AntV 是蚂蚁金服全新一代数据可视化解决方案。',
                  },*/
              ],
            },
          }
        }),
      },
    };
    delete props.dataSource;
    delete props.isMobile;
    const listChildren = this.getBlockChildren(dataSource.block.children);
    return (
      <div {...props} {...dataSource.wrapper}>
        <div {...dataSource.page}>
          <div {...dataSource.titleWrapper}>
            {dataSource.titleWrapper.children.map(getChildrenToRender)}
          </div>
          <OverPack {...dataSource.OverPack}>
            <QueueAnim key="tween" leaveReverse type="bottom">
              <QueueAnim
                key="block"
                type="bottom"
                {...dataSource.block}
                component={Row}
              >
                {listChildren}
              </QueueAnim>
            </QueueAnim>
          </OverPack>
        </div>
      </div>
    );
  }
}

export default Teams;
