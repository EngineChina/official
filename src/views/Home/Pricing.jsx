import React from 'react';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import QueueAnim from 'rc-queue-anim';
import { Row, Col, Button } from 'antd';
import { getChildrenToRender } from './utils';

class Pricing extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      //一个假数据
      data:[
        {
          name: "小程序起步价",
          money: 3000,
          content: "140-500Mbps<br /> 140 GB-50TB（含）<br /> 14500GB流量包<br /> 14国内按峰值宽带账单<br /> 14弹性计算<br /> 14云服务器 ECS",
          href: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1589135273978&di=3adf96baacc26e9cdfad5d3c0bf6b3bc&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F11bad3bf0d9976e774e442aff23b6d2d1d276ddb933f-6vxpJs_fw236"
        },{
          name: "官网起步价",
          money: 4999,
          content: "140-500Mbps<br /> 140 GB-50TB（含）<br /> 14500GB流量包<br /> 14国内按峰值宽带账单<br /> 14弹性计算<br /> 14云服务器 ECS",
          href: "https://www.zhihu.com/question/361166609"
        },{
          name: "数据可视化起步价",
          money: 6999,
          content: "140-500Mbps<br /> 140 GB-50TB（含）<br /> 14500GB流量包<br /> 14国内按峰值宽带账单<br /> 14弹性计算<br /> 14云服务器 ECS",
          href: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1589135273978&di=3adf96baacc26e9cdfad5d3c0bf6b3bc&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F11bad3bf0d9976e774e442aff23b6d2d1d276ddb933f-6vxpJs_fw236"
        },
      ]
    }
  }

  getChildrenToRender = (item) => {
    const {
      wrapper,
      topWrapper,
      name,
      buttonWrapper,
      line,
      content,
      money,
    } = item.children;
    return (
      <Col key={item.name} {...item}>
        <QueueAnim type="bottom" {...wrapper}>
          <div {...topWrapper}>
            <div {...name} key="name">
              {name.children}
            </div>
            <h1 {...money} key="money">
              {money.children}
            </h1>
          </div>
          <div {...content} key="content">
            {content.children}
          </div>
          <i {...line} key="line" />
          <div {...buttonWrapper} key="button">
            <Button {...buttonWrapper.children.a} />
          </div>
        </QueueAnim>
      </Col>
    );
  };

  render() {
    let that = this;
    const { ...props } = this.props;
    let dataSource = {
      wrapper: { className: 'home-page-wrapper pricing1-wrapper' },
      page: { className: 'home-page pricing1' },
      OverPack: { playScale: 0.3, className: 'pricing1-content-wrapper' },
      titleWrapper: {
        className: 'pricing1-title-wrapper',
        children: [
          { name: 'title', children: '价目表', className: 'pricing1-title-h1' },
        ],
      },
      block: {
        className: 'pricing1-block-wrapper',
        //根据data生成虚拟Dom
        children: that.state.data.map((v,i)=>{
          return {
            name: 'block'+i,
            className: 'pricing1-block',
            md: 8,
            xs: 24,
            children: {
              wrapper: { className: 'pricing1-block-box'+ (i===1?' active':''),},
              topWrapper: { className: 'pricing1-top-wrapper' },
              name: {
                className: 'pricing1-name',
                children: (
                  <span>
                <p>{v.name}</p>
              </span>
                ),
              },
              money: {
                className: 'pricing1-money',
                children: (
                  <span>
                    <p>¥{v.money}</p>
                  </span>
                ),
              },
              content: {
                className: 'pricing1-content',
                children: (
                  <span>
                    {v.content.split(/<br\/>|<br>|\n|<br \/>/).map((v,i)=>(<p key={i}>{v}</p>))}
                  </span>
                ),
              },
              line: { className: 'pricing1-line' },
              buttonWrapper: {
                className: 'pricing1-button-wrapper',
                children: {
                  a: {
                    className: 'pricing1-button',
                    href: v.href,
                    children: '立即购买',
                  },
                },
              },
            },
          }
        }),
      },
    };
    delete props.dataSource;
    delete props.isMobile;
    const { block } = dataSource;
    const childrenToRender = block.children.map(this.getChildrenToRender);
    return (
      <div {...props} {...dataSource.wrapper}>
        <div {...dataSource.page}>
          <div key="title" {...dataSource.titleWrapper}>
            {dataSource.titleWrapper.children.map(getChildrenToRender)}
          </div>
          <OverPack {...dataSource.OverPack}>
            <QueueAnim
              type="bottom"
              component={Row}
              leaveReverse
              ease={['easeOutQuad', 'easeInOutQuad']}
              key="content"
            >
              {childrenToRender}
            </QueueAnim>
          </OverPack>
        </div>
      </div>
    );
  }
}

export default Pricing;
