import React from "react"
import { CaretUpOutlined } from '@ant-design/icons';

import "./less/ToTop.less"

export default function ToTop(props) {
  const { showScroll } = props;
  window.onscroll = function () {
    document.getElementById("ToTop").style.display =
      document.documentElement.scrollTop>showScroll?
        "block":
        "none"
  };
  function toPageTop() {
    document.body.scrollIntoView({
      behavior: "smooth",
      block: "start"
    })
  }

  return(
    <div
      id={props.id}
      onClick={()=>{toPageTop()}}
      className="ToTopButton">
      <CaretUpOutlined/>
    </div>
  )
}
